#include "Mole.h"
#include <cstdlib>

Mole::Mole(sf::Texture& moleTexture, std::vector<sf::Sprite>& holes)
{
	// Assign our mole texture to the sprite
	sprite.setTexture(moleTexture);

	// Choose a random number that will dictate which hole to spawn from
	int chosenIndex = rand() % holes.size();

	// Set our sprite position based on the hole we chose
	sprite.setPosition(holes[chosenIndex].getPosition());

	// Set our points value to a specific number
	pointsValue = 100;

	// Set our initial time remaining for the mole (how long the mole should last)
	moleTimeRemaining = sf::seconds(5.0f);

	// Set how much time each mole should add to our time limit
	moleTimeToAdd = sf::seconds(1.0f);

}

void Mole::Update(sf::Time frameTime)
{
	// Update the time remaining for this mole before it should vanish
	// Subtract the time that passed this frame.
	moleTimeRemaining -= frameTime;

	// NOTE: Ideally we would clean up the mole here if it ran out of time, but that's a bit advanced. We'll do it in Main instead for now.
}