#pragma once
// Library needed for using sprites, textures, and fonts
#include <SFML/Graphics.hpp>
// Library for handling collections of objects
#include <vector>

// Definition of Mole class
class Mole
{
public: // access level (to be discussed later)

	// Constructor
	Mole(sf::Texture& moleTexture, std::vector<sf::Sprite>& holes);

	// Functions to call Player-specific code
	void Update(sf::Time frameTime);

	// Variables (data members) used by this class:
	sf::Sprite sprite;
	int pointsValue;
	sf::Time moleTimeRemaining;
	sf::Time moleTimeToAdd;
};

