// Library Includes	
// Library needed for using sprites, textures, and fonts
#include <SFML/Graphics.hpp>
// Library needed for playing music and sound effects
#include <SFML/Audio.hpp>
// Library for manipulating strings of text
#include <string>
// Library for handling collections of objects
#include <vector>
// Random numbers
#include <cstdlib>
#include <time.h>
// Include our own class definitions
#include "Player.h"
#include "Mole.h"

// The main() Function - entry point for our program
int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Whack-a-Mole", sf::Style::Titlebar | sf::Style::Close);

	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------
	// Player
	// Declare a texture variable called playerTexture
	sf::Texture playerTexture;
	// Load up our texture from a file path
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	// Declare a player object
	Player playerObject(playerTexture, gameWindow.getSize());

	// Holes
	// Declare a texture variable for the hole texture
	sf::Texture holeTexture;
	// Load the hole texture from file
	holeTexture.loadFromFile("Assets/Graphics/hole.png");
	// Declare a vector of hole sprites from which the moles will spawn.
	std::vector<sf::Sprite> holes;

	// Add and position the holes. We will use a for loop and MATHS
	// Figure out the centre point based on the window size, since we want our holes to be centred
	sf::Vector2u holeGridCentre(gameWindow.getSize().x / 2, gameWindow.getSize().y / 2);
	// How big is a single hole? Ask the texture!
	sf::Vector2u holeSize = holeTexture.getSize();
	// Decide how big we want the space between holes to be. We choose this. You can change it!
	sf::Vector2u holeSpacing(150, 150);
	// Determine the grid overall size based on the size of the holes and spacing, and the fact it is a 3 x 3 grid
	// Note: only total of 2 spacings between a total of 3 holes in a row / column
	sf::Vector2u holeGridSize(holeSize.x * 3 + holeSpacing.x * 2, holeSize.y * 3 + holeSpacing.y * 2); 
	// We need half the grid size...
	sf::Vector2u halfHoldGridSize(holeGridSize.x / 2, holeGridSize.y / 2);
	// We can get our top left corner by subtracting the half grid size from the centre (vector maths)
	sf::Vector2u holeGridTopLeft = holeGridCentre - halfHoldGridSize;

	// Now we can start creating and positioning our holes.
	// Use a nested for loop to control hole indices gridX and gridY (NOT THE SAME AS PIXEL POSITION) 
	for (int gridX = 0; gridX < 3; ++gridX)
	{
		// Executes three times
		// centre loop executes three times per outer loop.
		// sooooo.....
		for (int gridY = 0; gridY < 3; ++gridY)
		{
			// This centre part will execute once for each 9 elements in the 3 x 3 grid.
			// Each with a different x/y combination.

			// Calculate the position of our new hole
			// Start off assuming top left, then multiply our hole size and spacing by the number of holes we are to the right or down, and add that.
			sf::Vector2f holePosition;
			holePosition.x += holeGridTopLeft.x + (holeSize.x + holeSpacing.x) * gridX;
			holePosition.y += holeGridTopLeft.y + (holeSize.y + holeSpacing.y) * gridY;

			// First, create the new hole with correct texture and push it onto the vector
			holes.push_back(sf::Sprite(holeTexture));
			// Now set that hole's position - use holes.back to get the last element in the vector (the one we just made)
			holes.back().setPosition(holePosition);
		}
	}

	// Moles
	// Declare texture variable for moles
	sf::Texture moleTexture;
	// Load mole texture from file
	moleTexture.loadFromFile("Assets/Graphics/mole.png");
	// Create vector of moles (with nothing in it just yet)
	std::vector<Mole> moles;

	// Create a time value to store the total time between each item spawn
	sf::Time moleSpawnDuration = sf::seconds(3.0f);
	// Create a timer to store the time remaining for our game
	sf::Time moleSpawnRemaining = moleSpawnDuration;

	// Game Music
	// Declare a music variable called gameMusic
	sf::Music gameMusic;
	// Load up our audio from a file path
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	// Start our music playing
	gameMusic.play();

	// Game Font
	// Declare a font variable called gameFont
	sf::Font gameFont;
	// Load up the font from a file path
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");

	// Title Text
	// Declare a text variable called titleText to hold our game title display
	sf::Text titleText;
	// Set the font our text should use
	titleText.setFont(gameFont);
	// Set the string of text that will be displayed by this text object
	titleText.setString("Whack-a-Mole");
	// Set the size of our text, in pixels
	titleText.setCharacterSize(24);
	// Set the colour of our text
	titleText.setFillColor(sf::Color::Cyan);
	// Set the text style for the text
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// Position our text in the top center of the screen
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);

	// Author Text
	sf::Text authorText;
	authorText.setFont(gameFont);
	authorText.setString("by Sarah Herzog");
	authorText.setCharacterSize(16);
	authorText.setFillColor(sf::Color::Magenta);
	authorText.setStyle(sf::Text::Italic);
	authorText.setPosition(gameWindow.getSize().x / 2 - authorText.getLocalBounds().width / 2, 60);

	// Score
	// Declare an integer variable to hold our numerical score value
	int score = 0;
	// Setup score text object
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(30, 30);

	// Timer
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);
	// Create a time value to store the total time limit for our game
	sf::Time timeLimit = sf::seconds(10.0f);
	// Create a timer to store the time remaining for our game
	sf::Time timeRemaining = timeLimit;

	// Game Clock
	// Create a clock to track time passed each frame in the game
	sf::Clock gameClock;

	// Seed the random number generator
	srand(time(NULL));


	// Sound effects
	// Load the pickup sound effect file into a soundBuffer
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/pickup.wav");
	// Setup a Sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound pickupSound;
	pickupSound.setBuffer(pickupSoundBuffer);
	// Load the victory sound effect file into a soundBuffer
	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");
	// Setup a Sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound victorySound;
	victorySound.setBuffer(victorySoundBuffer);

	// Game over variable to track if the game is done
	bool gameOver = false;


	// Game Over Text
	// Declare a text variable called gameOverText to hold our game over display
	sf::Text gameOverText;
	// Set the font our text should use
	gameOverText.setFont(gameFont);
	// Set the string of text that will be displayed by this text object
	gameOverText.setString("GAME OVER\n\nress R to restart\nor Q to quit");
	// Set the size of our text, in pixels
	gameOverText.setCharacterSize(72);
	// Set the colour of our text
	gameOverText.setFillColor(sf::Color::Cyan);
	// Set the text style for the text
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// Position our text in the top center of the screen
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

	// Game Loop
	// Repeat as long as the window is open
	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;
		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This section will be repeated for each event waiting to be processed

			// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		} // End event polling loop

		// Player keybind input
		playerObject.Input();

		// Check if we should reset or quit the game
		if (gameOver)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
			{
				// Reset the game
				score = 0;
				timeRemaining = timeLimit;
				moles.clear();
				gameMusic.play();
				gameOver = false;
				playerObject.Reset(gameWindow.getSize());
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
			{
				gameWindow.close();
			}
		}

		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();
		// Update our time remaining based on how much time passed last frame
		timeRemaining = timeRemaining - frameTime;


		// Check if time has run out
		if (timeRemaining.asSeconds() <= 0)
		{
			// Don't let the time go lower than 0
			timeRemaining = sf::seconds(0);

			// Perform these actions only once when the game first ends:
			if (gameOver == false)
			{
				// Set our gameOver to true now so we don't perform these actions again
				gameOver = true;
				// Stop the main music from playing
				gameMusic.stop();
				// Play our victory sound
				victorySound.play();
			}
		}

		// Update our time display based on our time remaining
		timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));

		// Update our score display text based on our current numerical score
		scoreText.setString("Score: " + std::to_string(score));

		// Only perform this update logic if the game is still running:
		if (!gameOver)
		{
			// Update our mole spawn time remaining based on how much time passed last frame
			moleSpawnRemaining = moleSpawnRemaining - frameTime;
			// Check if time remaining to next spawn has reached 0
			if (moleSpawnRemaining <= sf::seconds(0.0f))
			{
				// Time to spawn a new mole!
				moles.push_back(Mole(moleTexture, holes));

				// Reset time remaining to full duration
				moleSpawnRemaining = moleSpawnDuration;
			}

			// Update the player
			playerObject.Update(frameTime);

			// Update the moles!
			for (int i = 0; i < moles.size(); ++i)
			{
				moles[i].Update(frameTime);
			}

			// Check for collisions
			for (int i = moles.size() - 1; i >= 0; --i)
			{
				sf::FloatRect itemBounds = moles[i].sprite.getGlobalBounds();
				sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();

				if (itemBounds.intersects(playerBounds))
				{
					// Our player touched the item!
					// Add the item's value to the score
					score += moles[i].pointsValue;
					// Add to player time remaining!
					timeRemaining += moles[i].moleTimeRemaining;
					// Remove the item from the vector
					moles.erase(moles.begin() + i);
					// Play the pickup sound
					pickupSound.play();
				}
			}

			// Check if the moles have run out of time
			for (int i = moles.size() - 1; i >= 0; --i)
			{
				if (moles[i].moleTimeRemaining.asSeconds() <= 0)
				{
					// The mole ran out of time! Delete it
					// Remove the item from the vector
					moles.erase(moles.begin() + i);
				}
			}
		}


		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------
		// Clear the window to a single colour
		gameWindow.clear(sf::Color(74,232,119));

		// Draw everything to the window
		gameWindow.draw(titleText);
		gameWindow.draw(authorText);
		gameWindow.draw(scoreText);
		gameWindow.draw(timerText);

		// Only draw these items if the game has NOT ended:
		if (!gameOver)
		{
			// Draw all our holes
			for (int i = 0; i < holes.size(); ++i)
			{
				gameWindow.draw(holes[i]);
			}

			// Draw all our moles
			for (int i = 0; i < moles.size(); ++i)
			{
				gameWindow.draw(moles[i].sprite);
			}

			gameWindow.draw(playerObject.sprite);
		}

		// Only draw these items if the game HAS ended:
		if (gameOver)
		{
			gameWindow.draw(gameOverText);
		}

		// Display the window contents on the screen
		gameWindow.display();

	} // End of Game Loop
	return 0;

} // End of main() Function